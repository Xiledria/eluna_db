CREATE TABLE IF NOT EXISTS `bug_report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `priority` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `createdBy` int(11) unsigned NOT NULL,
  `assignedTo` int(11) unsigned DEFAULT NULL,
  `resolvedBy` int(11) unsigned DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `bug_report_type` (`type`),
  KEY `bug_report_status` (`status`),
  KEY `bug_report_assignedTo` (`assignedTo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
