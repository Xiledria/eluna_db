ALTER TABLE `item_template` 
ADD COLUMN `bonding_realm_2` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0'
AFTER `bonding`;

UPDATE `item_template` SET bonding_realm_2 = bonding;
UPDATE `item_template` SET bonding_realm_2 = 1 where entry IN(select item from npc_vendor WHERE entry IN(select entry FROM creature_template WHERE subname LIKE "%bg vendor%"));
