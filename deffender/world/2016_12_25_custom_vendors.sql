-- cheaper shirts for daily tokens
UPDATE npc_vendor SET ExtendedCost = 2521 WHERE entry = 450196;
-- morph vendor
delete from npc_vendor where entry = 499999;
-- morph vendor
delete from creature where id IN(499999);
-- mount / pet vendors
UPDATE creature SET phaseMask = 32768 WHERE ID IN(450195, 450194, 450193, 400285, 400284);
-- transmog vendors - gonna be moved to event zone
DELETE from creature where ID IN(select entry from creature_template where name like "%transmog%");
DELETE FROM creature WHERE ID = 400001; -- transmogrifier
