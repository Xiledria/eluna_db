DELETE FROM spell_bonus_data WHERE entry IN (69770, 71044, 71045, 71046, 69099, 73776, 73777, 73778);
INSERT INTO spell_bonus_data (entry, direct_bonus, dot_bonus, ap_bonus, ap_dot_bonus, comments) VALUES
(69770, 0, 0, 0, 0, 'sindragosa - instability'),
(71044, 0, 0, 0, 0, 'sindragosa - instability'),
(71045, 0, 0, 0, 0, 'sindragosa - instability'),
(71046, 0, 0, 0, 0, 'sindragosa - instability'),
(69099, 0, 0, 0, 0, 'lich king - ice sphere - ice pulse'),
(73776, 0, 0, 0, 0, 'lich king - ice sphere - ice pulse'),
(73777, 0, 0, 0, 0, 'lich king - ice sphere - ice pulse'),
(73778, 0, 0, 0, 0, 'lich king - ice sphere - ice pulse');
