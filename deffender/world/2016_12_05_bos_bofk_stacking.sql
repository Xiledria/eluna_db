DELETE FROM `spell_group` WHERE `id` = 1123 AND `spell_id` = 67480;
INSERT INTO spell_group(id, spell_id) VALUES
(1124, 67480),
(1124, 72586);

DELETE FROM spell_group_stack_rules WHERE group_id IN (1123, 1124);
INSERT INTO spell_group_stack_rules (group_id, stack_rule) VALUES (1123, 1), (1124, 1);
