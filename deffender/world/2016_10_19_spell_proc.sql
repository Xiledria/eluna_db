-- nightfall procs
UPDATE `spell_proc` SET `SpellPhaseMask` = 2, `AttributesMask` = 2 WHERE `SpellId` = -18094;
UPDATE `spell_proc` SET `ProcFlags`= 2147483648, `SpellPhaseMask` = 1 WHERE `SpellId` = 17941;
-- Fel Domination
DELETE FROM `spell_proc` WHERE SpellId = 18708;
INSERT INTO `spell_proc` (`SpellId`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellTypeMask`, `SpellPhaseMask`) VALUES (18708, 5, 536870912, 4, 1);
-- grounding totem proc flags
UPDATE `spell_proc` SET `ProcFlags` = 2147483648 WHERE `SpellId` = 8178;

