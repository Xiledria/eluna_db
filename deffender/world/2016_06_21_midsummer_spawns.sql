SET @CGUID = 220627; -- 168 Required
SET @OGUID = 1145691; -- 419 Required
DELETE FROM `creature` WHERE `guid` BETWEEN @CGUID AND @CGUID+167;
DELETE FROM `gameobject` WHERE `guid` BETWEEN @OGUID AND @OGUID+418;
DELETE FROM `game_event_creature` WHERE `eventEntry`=1 AND `guid` BETWEEN @CGUID AND @CGUID+167;
DELETE FROM `game_event_gameobject` WHERE `eventEntry`=1 AND `guid` BETWEEN @OGUID AND @OGUID+418;
