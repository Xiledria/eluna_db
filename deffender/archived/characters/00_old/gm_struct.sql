CREATE TABLE IF NOT EXISTS `gm_struct` (
  `account_id` int(10) NOT NULL,
  `event_id` smallint(5) DEFAULT NULL,
  `usable_event_id_list` text,
  `load_last_event_id` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`account_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

