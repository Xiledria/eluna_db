DELIMITER $$

DROP PROCEDURE IF EXISTS upg_character_arena_stats $$
CREATE PROCEDURE upg_character_arena_stats()
BEGIN

IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='maxReachedMatchMakerRating' AND TABLE_NAME='character_arena_stats') ) THEN
    ALTER TABLE `character_arena_stats` ADD COLUMN maxReachedMatchMakerRating SMALLINT(5) NOT NULL AFTER matchMakerRating;
END IF;

END $$

CALL upg_character_arena_stats() $$

DELIMITER ;

