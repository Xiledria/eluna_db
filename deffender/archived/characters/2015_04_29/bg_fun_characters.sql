DELIMITER $$

DROP PROCEDURE IF EXISTS upgrade_bg_fun_characters $$
CREATE PROCEDURE upgrade_bg_fun_characters()
BEGIN

IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='realm_id' AND TABLE_NAME='characters') ) THEN
    ALTER TABLE `characters` ADD `realm_id` INT(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `deleteDate`;
END IF;

END $$

CALL upgrade_bg_fun_characters() $$

DELIMITER ;