REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (11366, -48108, 0, 'Hot Streak remove after cast rank 1');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (12505, -48108, 0, 'Hot Streak remove after cast rank 2');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (12522, -48108, 0, 'Hot Streak remove after cast rank 3');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (12523, -48108, 0, 'Hot Streak remove after cast rank 4');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (12524, -48108, 0, 'Hot Streak remove after cast rank 5');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (12525, -48108, 0, 'Hot Streak remove after cast rank 6');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (12526, -48108, 0, 'Hot Streak remove after cast rank 7');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (18809, -48108, 0, 'Hot Streak remove after cast rank 8');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (27132, -48108, 0, 'Hot Streak remove after cast rank 9');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (33938, -48108, 0, 'Hot Streak remove after cast rank 10');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (42890, -48108, 0, 'Hot Streak remove after cast rank 11');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (42891, -48108, 0, 'Hot Streak remove after cast rank 12');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (5143, -44401, 0, 'Missile Barrage remove after cast missiles rank1');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (5144, -44401, 0, 'Missile Barrage remove after cast missiles rank2');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (5145, -44401, 0, 'Missile Barrage remove after cast missiles rank3');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (5146, -44401, 0, 'Missile Barrage remove after cast missiles rank4');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (5147, -44401, 0, 'Missile Barrage remove after cast missiles rank5');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (10211, -44401, 0, 'Missile Barrage remove after cast missiles rank6');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (10212, -44401, 0, 'Missile Barrage remove after cast missiles rank7');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (25345, -44401, 0, 'Missile Barrage remove after cast missiles rank8');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (27075, -44401, 0, 'Missile Barrage remove after cast missiles rank9');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (38699, -44401, 0, 'Missile Barrage remove after cast missiles rank10');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (38704, -44401, 0, 'Missile Barrage remove after cast missiles rank11');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (42843, -44401, 0, 'Missile Barrage remove after cast missiles rank12');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (42846, -44401, 0, 'Missile Barrage remove after cast missiles rank13');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (44614, -57761, 0, 'Brain freez buff remove after cast frostfirebolt rank1');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (47610, -57761, 0, 'Brain freez buff remove after cast frostfirebolt rank2');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (133, -57761, 0, 'Brain freez buff remove after cast fireball rank1');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (143, -57761, 0, 'Brain freez buff remove after cast fireball rank2');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (145, -57761, 0, 'Brain freez buff remove after cast fireball rank3');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (3140, -57761, 0, 'Brain freez buff remove after cast fireball rank4');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (8400, -57761, 0, 'Brain freez buff remove after cast fireball rank5');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (8401, -57761, 0, 'Brain freez buff remove after cast fireball rank6');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (8402, -57761, 0, 'Brain freez buff remove after cast fireball rank7');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (10148, -57761, 0, 'Brain freez buff remove after cast fireball rank8');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (10149, -57761, 0, 'Brain freez buff remove after cast fireball rank9');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (10150, -57761, 0, 'Brain freez buff remove after cast fireball rank10');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (10151, -57761, 0, 'Brain freez buff remove after cast fireball rank11');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (25306, -57761, 0, 'Brain freez buff remove after cast fireball rank12');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (27070, -57761, 0, 'Brain freez buff remove after cast fireball rank13');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (38692, -57761, 0, 'Brain freez buff remove after cast fireball rank14');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (42832, -57761, 0, 'Brain freez buff remove after cast fireball rank15');
REPLACE INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES (42833, -57761, 0, 'Brain freez buff remove after cast fireball rank16');
