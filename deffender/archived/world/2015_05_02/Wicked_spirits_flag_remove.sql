UPDATE `creature_template` SET `AIName`='SmartAI' WHERE  `entry`=39190;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE  `entry`=39288;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE  `entry`=39287;
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE  `entry`=39289;
INSERT INTO `smart_scripts` (`entryorguid`, `id`, `event_type`, `action_type`, `action_param1`, `target_type`, `comment`) VALUES (39190, 1, 4, 19, 33554432, 1, 'remove flag');
INSERT INTO `smart_scripts` (`entryorguid`, `id`, `event_type`, `action_type`, `action_param1`, `target_type`, `comment`) VALUES (39287, 1, 4, 19, 33554432, 1, 'remove flag');
INSERT INTO `smart_scripts` (`entryorguid`, `id`, `event_type`, `action_type`, `action_param1`, `target_type`, `comment`) VALUES (39288, 1, 4, 19, 33554432, 1, 'remove flag');
INSERT INTO `smart_scripts` (`entryorguid`, `id`, `event_type`, `action_type`, `action_param1`, `target_type`, `comment`) VALUES (39289, 1, 4, 19, 33554432, 1, 'remove flag');
