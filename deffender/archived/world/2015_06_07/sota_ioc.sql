UPDATE `creature_template` SET `realm_id` = 0 WHERE `entry` IN
-- Strand of the Ancients
(29, 15214, 22515, 23472, 27894, 28781, 29260, 29262, 32795, 32796,
-- Isle of Conquest
34924,34922,34918,34919,34944,34775,35069,34776,34802,35273,34793,34935,34929,35003,34960,34984,20213,20212,
35415,35431,35413,35419,35407,35401,35405,35403,35410,35427,35429,35433,35421,36357,36358);

-- Isle of Conquest graveyard - should be now capturable
UPDATE `gameobject_template` SET `faction` = 84, `flags` = 32 WHERE `entry` = 195394;

-- Demolisher (1) - difficulty entry 1 had incorrect mechanic immunity masks
-- Alliance / Horde Gunship Cannon (1) had no mechanic immunity masks at all
UPDATE `creature_template` SET `mechanic_immune_mask` = 344276858 WHERE `entry` IN
(32796, 35410, 35427);

-- Disable regeneration for vehicles in Strand of the Ancients and Isle of Conquest
UPDATE `creature_template` SET `RegenHealth` = 0 WHERE entry IN
(27894,28781,32795,32796,34775,34776,34793,34802,34929,34935,34944,35069,35273,35410,35413,35415,35419,35421,35427,35429,35431,35433,36357,36358);

-- Spirit Healers on BG realm - difficulty entries
UPDATE creature_template SET realm_id = 0 WHERE entry IN(22526,31920,37236,22558,32004,37323);

-- Catapult moevment speed
UPDATE creature_template SET speed_run = speed_run / 1.5, speed_walk = speed_walk / 1.5 WHERE entry IN(34793, 35413);
